"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.insuredSchema = void 0;
const mongoose_1 = require("mongoose");
exports.insuredSchema = new mongoose_1.Schema({
    name: {
        type: String,
        required: true,
        uppercase: true,
        trim: true,
    },
    surname: {
        type: String,
        required: true,
        uppercase: true,
        trim: true,
    },
    dni: {
        type: String,
        unique: true,
        required: true,
        uppercase: true,
        trim: true,
    },
    date_of_birth: {
        type: Date,
        required: true,
        default: Date.now,
        min: "1900-01-01",
        max: Date.now,
    },
    gender: {
        type: String,
        required: true,
        uppercase: true,
        trim: true,
    },
    adress: {
        type: String,
        required: true,
        trim: true
    },
    city: {
        type: String,
        required: true,
        uppercase: true,
        trim: true
    },
    email: {
        type: String,
        default: '',
        required: false,
        uppercase: false,
        trim: true
    },
    phone: {
        type: String,
        required: true,
        uppercase: false,
        trim: true,
    },
    civil_state: {
        type: String,
        required: true,
        uppercase: true,
        trim: true,
    },
    occupation: {
        type: String,
        required: true,
        uppercase: true,
        trim: true,
    },
}, { timestamps: true });
// Campos Virtuales
exports.insuredSchema.virtual("fullName").get(function (name, surname) {
    return name + surname;
});
exports.default = mongoose_1.model("Insured", exports.insuredSchema);
