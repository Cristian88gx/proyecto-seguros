"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.officeSchema = void 0;
const mongoose_1 = require("mongoose");
exports.officeSchema = new mongoose_1.Schema({
    name: {
        type: String,
        required: true,
        trim: true,
    },
    adress: {
        type: String,
        required: true,
        trim: true,
    },
    email: {
        type: String,
        required: true,
        trim: true,
    },
}, { timestamps: true });
exports.default = mongoose_1.model("Office", exports.officeSchema);
