"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const policySchema = new mongoose_1.Schema({
    num_policy: {
        type: String,
        required: true,
        unique: true,
        uppercase: true,
        trim: true,
    },
    num_proposal: {
        type: String,
        required: true,
        unique: true,
        uppercase: true,
        trim: true,
    },
    section: {
        type: Number,
        required: true,
        trim: true,
    },
    date_init: {
        type: Date,
        required: true,
        default: Date.now,
        min: Date.now,
    },
    date_finish: {
        type: Date,
        required: true,
        default: Date.now,
    },
    date_reception: {
        type: Date,
        default: Date.now,
    },
    date_hight: {
        type: Date,
        required: false,
        default: Date.now,
    },
    num_cuotes: {
        type: Number,
        required: true,
        trim: true,
    },
    amount_total: {
        type: Number,
        required: true,
        trim: true,
    },
    state: {
        type: String,
        required: true,
        default: "en tramite",
        uppercase: true,
        trim: true,
    },
    observation: {
        type: String,
        required: true,
        uppercase: true,
        trim: true,
    },
    vehicle: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: "Vehicle",
        required: true,
    },
    insured: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: "Insured",
        required: true,
    },
    office: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: "Office",
        required: true,
    },
    cuotes: [
        {
            type: mongoose_1.Schema.Types.ObjectId,
            ref: "Cuote",
            default: [],
            required: false,
        },
    ],
}, { timestamps: true });
// Static methods
policySchema.statics.findMyVehicle = function (id) {
    return __awaiter(this, void 0, void 0, function* () {
        return this.findById(id).populate("Vehicle").exec();
    });
};
policySchema.statics.findMyOffice = function (id) {
    return __awaiter(this, void 0, void 0, function* () {
        return this.findById(id).populate("Office").exec();
    });
};
exports.default = mongoose_1.model("Policy", policySchema);
