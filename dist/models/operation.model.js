"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const operationSchema = new mongoose_1.Schema({
    date_of_payment: {
        type: Date,
        required: true,
    },
    hour_of_expiration: {
        type: Date,
        required: true,
    },
    amount: {
        type: Number,
        required: true
    },
    policy_id: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: "Policy",
        required: true
    },
    cuotes: [
        {
            type: mongoose_1.Schema.Types.ObjectId,
            ref: "Policy",
        },
    ]
}, { timestamps: true });
exports.default = mongoose_1.model("Operation", operationSchema);
