"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.vehicleSchema = void 0;
const mongoose_1 = require("mongoose");
exports.vehicleSchema = new mongoose_1.Schema({
    brand: {
        type: String,
        required: true,
        uppercase: true,
        trim: true,
    },
    model_vehicle: {
        type: String,
        required: true,
        uppercase: true,
        trim: true,
    },
    year: {
        type: Number,
        required: true,
        trim: true,
    },
    domain: {
        type: String,
        unique: true,
        required: true,
        uppercase: true,
        trim: true,
    },
    num_chasis: {
        type: String,
        unique: true,
        uppercase: true,
        required: true,
        trim: true,
    },
    num_motor: {
        type: String,
        unique: true,
        uppercase: true,
        required: true,
        trim: true,
    },
    type_vehicle: {
        type: String,
        required: true,
        uppercase: true,
        trim: true,
    },
    uses: {
        type: String,
        required: true,
        uppercase: true,
        trim: true,
    },
    color: {
        type: String,
        required: true,
        uppercase: true,
        trim: true,
    },
    holder: {
        type: String,
        required: true,
        uppercase: true,
        trim: true,
    },
    observation: {
        type: String,
        required: false,
        default: "",
        uppercase: true,
        trim: true,
    },
    insured: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: "Insured",
        required: true,
    },
}, { timestamps: true });
// Static methods
exports.vehicleSchema.statics.findMyInsured = function (dni) {
    return this.findOne(dni).populate("Insured").exec();
};
exports.default = mongoose_1.model("Vehicle", exports.vehicleSchema);
