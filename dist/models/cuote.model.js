"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const cuoteSchema = new mongoose_1.Schema({
    import: {
        type: Number,
        required: true,
    },
    date_of_expiration: {
        type: Date,
        required: true,
        min: "1900-01-01",
    },
    paid_out: {
        type: Boolean,
        required: true,
        default: false,
    },
    policy_id: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: "Policy",
        required: true
    },
}, { timestamps: true });
exports.default = mongoose_1.model("Cuote", cuoteSchema);
