"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
// Schema
const UserSchema = new mongoose_1.Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: String,
    username: {
        type: String,
        unique: true,
        required: true,
        lowercase: true
    },
    password: {
        type: String,
        required: true
    },
    company: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: "Company",
        required: true
    },
    gender: {
        type: Number,
        enum: [0, 1],
        default: 0,
        required: true
    },
    friends: [{
            type: String,
        }],
    creditCards: {
        type: Map,
        of: String
    }
});
var Gender;
(function (Gender) {
    Gender[Gender["Male"] = 1] = "Male";
    Gender[Gender["Female"] = 0] = "Female";
})(Gender || (Gender = {}));
// Virtuals
// Methods
UserSchema.methods.getGender = function () {
    return this.gender > 0 ? "Male" : "Female";
};
// Static methods
UserSchema.statics.findMyCompany = function (id) {
    return __awaiter(this, void 0, void 0, function* () {
        return this.findById(id).populate("company").exec();
    });
};
// Default export
exports.default = mongoose_1.model("Userr", UserSchema);
