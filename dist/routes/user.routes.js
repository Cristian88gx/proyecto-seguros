"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const passport_1 = __importDefault(require("passport"));
const user_controller_1 = require("../controllers/user.controller");
const userController = new user_controller_1.UserController();
const router = express_1.Router();
const autenticateJwt = passport_1.default.authenticate("jwt", { session: false });
// Crear un usuario nuevo
router.post("/createUser", userController.addNewUser);
// Obtener todos los usuarios
router.get("/getUsers", userController.getUsers);
// Buscar un usuario por su id
router.get("/searchUser/:userId", userController.getUserWithId);
// Modificar un usuario
router.put("/updateUser/:userId", userController.updateUser);
// Eliminar un usuario por su id
router.delete("/deleteUser/:userId", userController.deleteUser);
exports.default = router;
