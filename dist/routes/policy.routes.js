"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const passport_1 = __importDefault(require("passport"));
const role_1 = __importDefault(require("../middlewares/role"));
const policy_controller_1 = require("../controllers/policy.controller");
const policyController = new policy_controller_1.PolicyController();
const router = express_1.Router();
const autenticateJwt = passport_1.default.authenticate("jwt", { session: false });
// Crear una poliza nueva
router.post("/createPolicy/:idVehicle", [autenticateJwt, role_1.default(["admin", "superadmin"])], policyController.addNewPolicy);
// Obtener todas las polizas
router.get("/getPolicies", [autenticateJwt, role_1.default(["admin", "superadmin"])], policyController.getPolicies);
// Buscar una poliza por su id
router.get("/searchPolicy/:policyId", [autenticateJwt, role_1.default(["admin", "superadmin"])]);
// Modificar una poliza
router.put("/updateOffice/:officeId", [autenticateJwt, role_1.default(["admin", "superadmin"])]);
// Eliminar una poliza por su id
router.delete("/deletePolicy/:policyId", [autenticateJwt, role_1.default(["admin", "superadmin"])]);
exports.default = router;
