"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const passport_1 = __importDefault(require("passport"));
const role_1 = __importDefault(require("../middlewares/role"));
const office_controller_1 = require("../controllers/office.controller");
const officeController = new office_controller_1.OfficeController();
const router = express_1.Router();
const autenticateJwt = passport_1.default.authenticate("jwt", { session: false });
// Crear una oficina nueva
router.post("/createOffice", 
//[autenticateJwt, checkRole(["admin", "superadmin"])],
officeController.addNewOffice);
// Obtener todas las oficinas
router.get("/getOffices", 
//[autenticateJwt, checkRole(["admin", "superadmin"])],
officeController.getOffices);
// Buscar una oficina por su id
router.get("/searchOffice/:officeId", [autenticateJwt, role_1.default(["admin", "superadmin"])], officeController.getOfficeWithId);
// Modificar una oficina
router.put("/updateOffice/:officeId", [autenticateJwt, role_1.default(["admin", "superadmin"])], officeController.updateOffice);
// Eliminar una oficina por su id
router.delete("/deleteOffice/:officeId", [autenticateJwt, role_1.default(["admin", "superadmin"])], officeController.deleteOffice);
exports.default = router;
