"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const passport_1 = __importDefault(require("passport"));
const role_1 = __importDefault(require("../middlewares/role"));
const vehicle_controller_1 = require("../controllers/vehicle.controller");
const vehicleController = new vehicle_controller_1.VehicleController();
const router = express_1.Router();
const autenticateJwt = passport_1.default.authenticate("jwt", { session: false });
// Crear un vehiculo nuevo
router.post("/createVehicle/:dniInsured", vehicleController.addNewVehicle);
// Obtener todos los vehiculos
router.get("/getVehicles", vehicleController.getVehicles);
// Buscar un vehiculo por su dominio
router.get("/searchVehicle/:vehicleDomain", [autenticateJwt, role_1.default(["admin", "user", "superadmin"])], vehicleController.getVehicleWithDomain);
// Buscar vehiculos de un asegurado
router.get("/searchVehicles/:IdInsured", vehicleController.getVehiclesWithInsured);
// Buscar un vehiculo por su titular
router.get("/searchVehicle/:vehicleHolder", [autenticateJwt, role_1.default(["admin", "user", "superadmin"])], vehicleController.getVehiclesWithHolder);
// Buscar vehiculos por fecha de creacion
router.get("/searchVehiclesCreated/:date", [autenticateJwt, role_1.default(["admin", "superadmin"])], vehicleController.getVehiclesWithDateCreated);
// Buscar vehiculos por Marca
router.get("/searchVehiclesBrand/:brand", [autenticateJwt, role_1.default(["admin", "superadmin"])], vehicleController.getVehiclesWithBrand);
// Modificar un vehiculo (basico)
router.put("/updateVehicleBasic/:vehicleId", [autenticateJwt, role_1.default(["admin", "user", "superadmin"])], vehicleController.updateVehicleBasic);
// Modificar un vehiculo
router.put("/updateVehicle/:vehicleId", [autenticateJwt, role_1.default(["admin", "user", "superadmin"])], vehicleController.updateVehicle);
// Eliminar un vehiculo por su id
router.delete("/deleteVehicle/:vehicleId", vehicleController.deleteVehicle);
exports.default = router;
