"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const passport_1 = __importDefault(require("passport"));
const role_1 = __importDefault(require("../middlewares/role"));
const insured_controller_1 = require("../controllers/insured.controller");
const insuredController = new insured_controller_1.InsuredController();
const router = express_1.Router();
const autenticateJwt = passport_1.default.authenticate("jwt", { session: false });
// Crear un asegurado nuevo
router.post("/createInsured", insuredController.addNewInsured);
// Obtener todos los asegurados
router.get("/getInsureds", insuredController.getInsureds);
// Buscar un asegurado por su dni
router.get("/searchInsured/:insuredDni", [autenticateJwt, role_1.default(["admin", "user", "superadmin"])], insuredController.getInsuredWithDni);
// Buscar un asegurado por su apellido
router.get("/searchInsureds/:insuredSurname", [autenticateJwt, role_1.default(["admin", "user", "superadmin"])], insuredController.getInsuredsWithSurname);
// Buscar asegurados por fecha de creacion
router.get("/searchInsureds/:date", [autenticateJwt, role_1.default(["admin", "user", "superadmin"])], insuredController.getInsuredsWithDateCreated);
// Modificar un asegurado
router.put("/updateInsured/:insuredId", [autenticateJwt, role_1.default(["admin", "user", "superadmin"])], insuredController.updateInsured);
// Eliminar un usuario por su id
router.delete("/deleteInsured/:insuredId", insuredController.deleteInsured);
exports.default = router;
