"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const multer_1 = __importDefault(require("multer")); // es un modulo que me permite guardar imagenes
const uuid_1 = require("uuid"); // es un modulo que devuelve una cadena aleatoria
const path_1 = __importDefault(require("path")); // maneja la ubicacion de los archivos y sus extensiones
const storage = multer_1.default.diskStorage({
    destination: 'uploads',
    filename: (req, file, cb) => {
        cb(null, uuid_1.v4() + path_1.default.extname(file.originalname)); // le asigno un nombre aleatorio a la imagen y su extension
    }
});
exports.default = multer_1.default({ storage });
