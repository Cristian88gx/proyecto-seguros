"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OfficeController = void 0;
const cuote_model_1 = __importDefault(require("../models/cuote.model"));
class OfficeController {
    constructor() {
        //Agregar oficina
        this.addNewCuote = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const cuoteCreated = new cuote_model_1.default(req.body);
                if (!cuoteCreated.import ||
                    !cuoteCreated.date_of_expiration ||
                    !cuoteCreated.paid_out) {
                    // Verifico si estan todos los datos
                    return res.status(400).json({ msg: "Debe Ingresar todos los datos" });
                }
                const office = yield Office.findOne({ name: req.body.name }); // Busco en la bd si ya esta registrado
                if (office) {
                    //Verifico si ya existe
                    return res.status(404).json({ msg: "La oficina ya esta registrada" });
                }
                const newOffice = new Office({
                    name: officeCreated.name,
                    adress: officeCreated.adress,
                    email: officeCreated.email,
                }); // Creo una nueva oficina y le asigno el body del reques
                yield newOffice.save();
                return res.status(201).json({
                    // devuelvo el status y la info de la nueva oficina
                    name: newOffice.name,
                    adress: newOffice.adress,
                    email: newOffice.email,
                    message: "Oficina Creada!",
                    success: true,
                });
            }
            catch (err) {
                console.log(err);
                return res.status(500).json({
                    message: "Error creando la oficina.",
                    success: false,
                });
            }
        });
        //Traer todas las oficinas
        this.getOffices = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const offices = yield Office.find(); // guardo en un arreglo las oficinas de la bd
            return res.json(offices);
        });
        //Consultar una oficina por id
        this.getOfficeWithId = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { officeId } = req.params; // guardo el id en una constante
            const office = yield Office.findById(officeId); // busco en la bd
            if (!office) {
                return res.status(404).json("No Office found");
            }
            return res.json(office);
        });
        //Modificar una oficina
        this.updateOffice = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const { officeId } = req.params;
                const { name, adress, email } = req.body;
                if (!name || !adress || !email) {
                    // Verifico si me mandan los campos obligatorios
                    return res.status(400).json({ msg: "Debe completar todos los campos" });
                }
                const updatedOffice = yield Office.findByIdAndUpdate(officeId, {
                    // busco y actualizo en la bd
                    name,
                    adress,
                    email,
                }, { new: true }); // para que no me devuelva la oficina anterior sino la nueva modificada
                return res.status(200).json({
                    message: "Datos Modificados",
                    updatedOffice,
                });
            }
            catch (err) {
                console.log(err);
                return res.status(500).json({
                    message: "Error actualizando la oficina.",
                    success: false,
                });
            }
        });
        //Eliminar una oficina
        this.deleteOffice = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const { officeId } = req.params;
                const office = yield Office.findByIdAndRemove(officeId);
                if (!officeId || !office) {
                    return res.status(400).json({
                        message: "Debe seleccionar una oficina",
                    });
                }
                return res.status(200).json({
                    message: "Oficina eliminada",
                    office,
                });
            }
            catch (err) {
                console.log(err);
                return res.status(500).json({
                    message: "Error eliminando la oficina.",
                    success: false,
                });
            }
        });
    }
}
exports.OfficeController = OfficeController;
