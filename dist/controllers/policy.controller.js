"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PolicyController = void 0;
const vehicle_model_1 = __importDefault(require("../models/vehicle.model"));
const policy_model_1 = __importDefault(require("../models/policy.model"));
const cuote_model_1 = __importDefault(require("../models/cuote.model"));
const office_model_1 = __importDefault(require("../models/office.model"));
class PolicyController {
    constructor() {
        //Crear poliza
        this.addNewPolicy = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const { num_policy, num_proposal, section, date_init, date_finish, num_cuotes, amount_total, observation } = req.body;
                const { idVehicle } = req.params;
                const { idOffice } = req.params;
                if (!num_policy ||
                    !num_proposal ||
                    !section ||
                    !date_init ||
                    !date_finish ||
                    !num_cuotes ||
                    !amount_total ||
                    !observation) {
                    // Verifico si me mandan los campos obligatorios
                    return res.status(400).json({ msg: "Debe Ingresar todos los datos" });
                }
                const vehicle = yield vehicle_model_1.default.findOne({ _id: idVehicle }); // Busco en la bd si ya esta registrado el vehiculo
                const office = yield office_model_1.default.findOne({ _id: idOffice }).populate('office', 'name');
                if (!vehicle) {
                    //Verifico que el vehiculo y el asegurado exista
                    return res.status(404).json({ msg: "No existe el vehiculo" });
                }
                if (!office) {
                    //Verifico que la oficina exista
                    return res.status(404).json({ msg: "No existe la oficina" });
                }
                const policy = yield policy_model_1.default.findOne({ num_policy: num_policy }); // Busco en la bd si ya esta registrada la poliza
                if (policy) {
                    //Verifico que el la poliza no este registrada ya
                    return res.status(404).json({ msg: "Ya existe la poliza" });
                }
                const newPolicy = new policy_model_1.default({
                    num_policy: num_policy,
                    num_proposal: num_proposal,
                    section: section,
                    date_init: date_init,
                    date_finish: date_finish,
                    num_cuotes: num_cuotes,
                    amount_total: amount_total,
                    observation: observation,
                    vehicle: vehicle,
                    insured: vehicle.insured,
                    office: office,
                    cuotes: this.createCuotes(num_cuotes, amount_total, num_policy, date_init),
                });
                yield newPolicy.save();
                return res.status(201).json({
                    // devuelvo el status y la info del nuevo vehiculo
                    num_policy: newPolicy.num_policy,
                    num_proposal: newPolicy.num_proposal,
                    section: newPolicy.section,
                    date_init: newPolicy.date_init,
                    date_finish: newPolicy.date_finish,
                    date_high: newPolicy.date_high,
                    num_cuotes: newPolicy.num_cuotes,
                    amount_total: newPolicy.amount_total,
                    observation: newPolicy.observation,
                    vehicle: newPolicy.vehicle,
                    insured: newPolicy.insured,
                    office: newPolicy.office,
                    cuotes: newPolicy.cuotes,
                    message: "Poliza Creada!",
                    success: true,
                });
            }
            catch (err) {
                console.log(err);
                return res.status(500).json({
                    message: "Error creando la poliza",
                    success: false,
                });
            }
        });
        //Crear cuotas de una poliza
        this.createCuotes = (cuotes, total, policy, init) => {
            let arr_cuotes = new Array(cuotes);
            let import_cuote = total / cuotes;
            let lastDate = init;
            for (let i = 1; i <= cuotes; i++) {
                lastDate.setDate(lastDate.getDate() + 30);
                const cuoteCreated = new cuote_model_1.default({
                    import: import_cuote,
                    date_of_expiration: lastDate,
                    paid_out: false,
                    policy_id: policy,
                });
                arr_cuotes[i] = cuoteCreated;
            }
            return arr_cuotes;
        };
        //Consultar todas las polizas
        this.getPolicies = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const policies = yield policy_model_1.default.find(); // guardo en un arreglo las polizas de la bd
                return res.json(policies);
            }
            catch (err) {
                console.log(err);
                return res.status(500).json({
                    message: "Error consultando las polizas",
                    success: false,
                });
            }
        });
        //Consultar una poliza por numero de poliza
        this.getPolicyForNumPolicy = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const { policyNum } = req.params; // guardo el numero de poliza en una constante
                const policy = yield policy_model_1.default.findOne({ num_policy: policyNum }); // busco en la bd
                if (!policy) {
                    return res.status(404).json("No se encontro la poliza");
                }
                return res.status(201).json(policy);
            }
            catch (err) {
                console.log(err);
                return res.status(500).json({
                    message: "Error consultando las poliza",
                    success: false,
                });
            }
        });
        //Consultar una poliza por numero de propuesta
        this.getPolicyForNumProposal = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const { policyPro } = req.params; // guardo el numero de propuesta en una constante
                const policy = yield policy_model_1.default.findOne({ num_proposal: policyPro }); // busco en la bd
                if (!policy) {
                    return res.status(404).json("No se encontro la poliza");
                }
                return res.status(201).json(policy);
            }
            catch (err) {
                console.log(err);
                return res.status(500).json({
                    message: "Error consultando la poliza",
                    success: false,
                });
            }
        });
        //Consultar polizas por fecha de alta
        this.getVehiclesForDateHight = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                if (req.params == null) {
                    return res.status(400).json({
                        message: "Debe pasar una fecha",
                    });
                }
                const { date } = req.params;
                const nextDate = new Date(date);
                const dateHigh = new Date(date);
                nextDate.setDate(dateHigh.getDate() + 1);
                const policies = yield vehicle_model_1.default.find({
                    createdAt: { $gte: dateHigh, $lt: nextDate },
                });
                if (policies.length === 0) {
                    return res.status(400).json({
                        message: "No hay polizas",
                    });
                }
                return res.status(200).json(policies);
            }
            catch (err) {
                console.log(err);
                return res.status(500).json({
                    message: "Error buscando polizas por fecha de creacion",
                    success: false,
                });
            }
        });
        //Consultar polizas por asegurado
        this.getPoliciesForInsured = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const { IdInsured } = req.params;
                const vehicles = yield policy_model_1.default.find(); // guardo en un arreglo el o los asegurados que coincidan con el apellido
                return res.json(vehicles);
            }
            catch (err) {
                console.log(err);
                return res.status(500).json({
                    message: "Error buscando vehiculos por asegurado.",
                    success: false,
                });
            }
        });
        //Modificar una poliza
        this.updatePolicy = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const { vehicleId } = req.params;
                const { brand, model_vehicle, year, domain, num_chasis, num_motor, type_vehicle, uses, color, holder, observation, insured, } = req.body;
                if (!brand ||
                    !model_vehicle ||
                    !year ||
                    !domain ||
                    !num_chasis ||
                    !num_motor ||
                    !type_vehicle ||
                    !uses ||
                    !color ||
                    !holder ||
                    !observation ||
                    !insured) {
                    // Verifico si me mandan los campos obligatorios
                    return res.status(400).json({ msg: "Debe completar todos los campos" });
                }
                const updatedVehicle = yield vehicle_model_1.default.findByIdAndUpdate(vehicleId, {
                    // busco y actualizo en la bd
                    brand,
                    model_vehicle,
                    year,
                    domain,
                    num_chasis,
                    num_motor,
                    type_vehicle,
                    uses,
                    color,
                    holder,
                    observation,
                    insured,
                }, { new: true }); // para que no me devuelva el asegurado anterior sino el nuevo modificado
                return res.status(200).json({
                    message: "Datos Modificados",
                    updatedVehicle,
                });
            }
            catch (err) {
                console.log(err);
                return res.status(500).json({
                    message: "Error actualizando el vehiculo.",
                    success: false,
                });
            }
        });
        //Eliminar un vehiculo
        this.deleteVehicle = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const { vehicleId } = req.params;
                const vehicle = yield vehicle_model_1.default.findByIdAndRemove(vehicleId);
                if (vehicle) {
                    return res.status(200).json({
                        message: "Asegurado eliminado",
                        vehicle,
                    });
                }
            }
            catch (err) {
                console.log(err);
                return res.status(500).json({
                    message: "Error eliminando el asegurado.",
                    success: false,
                });
            }
        });
    }
}
exports.PolicyController = PolicyController;
