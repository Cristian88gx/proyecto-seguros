"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InsuredController = void 0;
const insured_model_1 = __importDefault(require("../models/insured.model"));
class InsuredController {
    constructor() {
        //Agregar asegurado
        this.addNewInsured = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const insuredCreated = new insured_model_1.default(req.body);
                if (!insuredCreated.name ||
                    !insuredCreated.surname ||
                    !insuredCreated.dni ||
                    !insuredCreated.date_of_birth ||
                    !insuredCreated.gender ||
                    !insuredCreated.adress ||
                    !insuredCreated.city ||
                    !insuredCreated.phone ||
                    !insuredCreated.civil_state ||
                    !insuredCreated.occupation) {
                    // Verifico si me mandan los campos obligatorios
                    return res.status(400).json({ msg: "Debe Ingresar todos los datos" });
                }
                const insured = yield insured_model_1.default.findOne({ dni: req.body.dni }); // Busco en la bd si ya esta registrado el asegurado
                if (insured) {
                    //Verifico que el asegurado no este registrado ya
                    return res.status(404).json({ msg: "El asegurado ya esta registrado" });
                }
                const newInsured = new insured_model_1.default({
                    name: insuredCreated.name,
                    surname: insuredCreated.surname,
                    dni: insuredCreated.dni,
                    date_of_birth: insuredCreated.date_of_birth,
                    gender: insuredCreated.gender,
                    adress: insuredCreated.adress,
                    email: insuredCreated.email,
                    city: insuredCreated.city,
                    phone: insuredCreated.phone,
                    civil_state: insuredCreated.civil_state,
                    occupation: insuredCreated.occupation,
                }); // Creo un nuevo asegurado y le asigno el body del request      await newUser.save();
                yield newInsured.save();
                return res.status(201).json({
                    // devuelvo el status y la info del nuevo asegurado
                    name: newInsured.name,
                    surname: newInsured.surname,
                    dni: newInsured.dni,
                    date_of_birth: newInsured.date_of_birth,
                    gender: newInsured.gender,
                    adress: newInsured.adress,
                    city: newInsured.city,
                    phone: newInsured.phone,
                    civil_state: newInsured.civil_state,
                    occupation: newInsured.occupation,
                    email: newInsured.email,
                    message: "Registro Completo!",
                    success: true,
                });
            }
            catch (err) {
                console.log(err);
                return res.status(500).json({
                    message: "Error creando el asegurado.",
                    success: false,
                });
            }
        });
        //Traer todos los asegurados
        this.getInsureds = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const insureds = yield insured_model_1.default.find(); // guardo en un arreglo los usuarios de la bd
                return res.json(insureds);
            }
            catch (err) {
                console.log(err);
                return res.status(500).json({
                    message: "Error consultando los asegurados",
                    success: false,
                });
            }
        });
        //Consultar un asegurado por dni
        this.getInsuredWithDni = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const { insuredDni } = req.params; // guardo el id en una constante
                const insured = yield insured_model_1.default.findOne({ dni: insuredDni }); // busco en la bd
                if (!insured) {
                    return res.status(404).json("No se encontro el asegurado");
                }
                return res.json(insured);
            }
            catch (err) {
                console.log(err);
                return res.status(500).json({
                    message: "Error consultando el asegurado",
                    success: false,
                });
            }
        });
        //Consultar asegurados por apellido
        this.getInsuredsWithSurname = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const { insuredSurname } = req.params;
                const insureds = yield insured_model_1.default.find({ surname: insuredSurname }); // guardo en un arreglo el o los asegurados que coincidan con el apellido
                return res.json(insureds);
            }
            catch (err) {
                console.log(err);
                return res.status(500).json({
                    message: "Error buscando asegurados por apellido.",
                    success: false,
                });
            }
        });
        //Consultar asegurados por fecha de creacion
        this.getInsuredsWithDateCreated = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const { date } = req.params;
                if (!date)
                    return res.status(400).json({
                        message: "Debe pasar una fecha",
                    });
                const insureds = yield insured_model_1.default.find({ createdAt: date }); // guardo en un arreglo el o los asegurados que coincidan con el apellido
                return res.status(200).json(insureds);
            }
            catch (err) {
                console.log(err);
                return res.status(500).json({
                    message: "Error buscando asegurados por fecha de creacion",
                    success: false,
                });
            }
        });
        //Modificar un asegurado
        this.updateInsured = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const { insuredId } = req.params;
                const { name, surname, dni, date_of_birth, gender, adress, email, city, phone, civil_state, occupation, } = req.body;
                if (!name ||
                    !surname ||
                    !dni ||
                    !date_of_birth ||
                    !gender ||
                    !adress ||
                    !city ||
                    !phone ||
                    !civil_state ||
                    !occupation) {
                    // Verifico si me mandan los campos obligatorios
                    return res.status(400).json({ msg: "Debe completar todos los campos" });
                }
                const updatedInsured = yield insured_model_1.default.findByIdAndUpdate(insuredId, {
                    // busco y actualizo en la bd
                    name,
                    surname,
                    dni,
                    date_of_birth,
                    gender,
                    adress,
                    city,
                    email,
                    phone,
                    civil_state,
                    occupation,
                }, { new: true }); // para que no me devuelva el asegurado anterior sino el nuevo modificado
                return res.status(200).json({
                    message: "Datos Modificados",
                    updatedInsured,
                });
            }
            catch (err) {
                console.log(err);
                return res.status(500).json({
                    message: "Error actualizando el asegurado.",
                    success: false,
                });
            }
        });
        //Eliminar un asegurado
        this.deleteInsured = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const { insuredId } = req.params;
                const insured = yield insured_model_1.default.findByIdAndRemove(insuredId);
                if (insured) {
                    return res.status(200).json({
                        message: "Asegurado eliminado",
                        insured,
                    });
                }
            }
            catch (err) {
                console.log(err);
                return res.status(500).json({
                    message: "Error eliminando el asegurado.",
                    success: false,
                });
            }
        });
    }
}
exports.InsuredController = InsuredController;
