"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const user_model_1 = __importDefault(require("../models/user.model"));
const fs_extra_1 = __importDefault(require("fs-extra")); // Este modulo permite trabajar con archivos
const path_1 = __importDefault(require("path")); // Este modulo maneja las direcciones de los archivos
const encryptPassword_1 = __importDefault(require("../utils/encryptPassword"));
class UserController {
    constructor() {
        //Agregar usuario
        this.addNewUser = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const userCreated = new user_model_1.default(req.body);
                console.log("entro");
                if (!userCreated.username ||
                    !userCreated.password ||
                    !userCreated.name ||
                    !userCreated.role ||
                    !userCreated.email) {
                    // Verifico si el usuario me manda usuario y password
                    return res.status(400).json({ msg: "Debe Ingresar todos los datos" });
                }
                const user = yield user_model_1.default.findOne({ username: req.body.username }); // Busco en la bd si ya esta registrado el usuario
                if (user) {
                    //Verifico que el usuario no este registrado ya
                    return res.status(404).json({ msg: "El usuario ya esta registrado" });
                }
                if (req.file !== undefined) {
                    console.log("entro con foto");
                    // Si se proporciona una foto de perfil
                    const newUser = new user_model_1.default({
                        name: userCreated.name,
                        username: userCreated.username,
                        email: userCreated.email,
                        password: userCreated.password,
                        role: userCreated.role,
                        imagePath: req.file.path,
                    }); // Creo un nuevo usuario y le asigno el body del request
                    console.log(newUser);
                    yield newUser.save();
                }
                console.log("entro sin foto");
                const newUser = new user_model_1.default({
                    name: userCreated.name,
                    username: userCreated.username,
                    email: userCreated.email,
                    password: userCreated.password,
                    role: userCreated.role,
                    imagePath: "",
                }); // Creo un nuevo usuario y le asigno el body del request
                console.log(newUser);
                yield newUser.save();
                return res.status(201).json({
                    // devuelvo el status y la info del nuevo usuario
                    name: newUser.name,
                    username: newUser.username,
                    role: newUser.role,
                    email: newUser.email,
                    imagePath: newUser.imagePath,
                    message: "Registro Completo!",
                    success: true,
                });
            }
            catch (err) {
                console.log(err);
                return res.status(500).json({
                    message: "Error creando el usuario.",
                    success: false,
                });
            }
        });
        //Traer todos los usuarios
        this.getUsers = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const users = yield user_model_1.default.find(); // guardo en un arreglo los usuarios de la bd
            return res.json(users);
        });
        //Consultar un usuario por id
        this.getUserWithId = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { userId } = req.params; // guardo el id en una constante
            console.log(userId);
            const user = yield user_model_1.default.findById(userId); // busco en la bd
            if (!user) {
                return res.status(404).json("No User found");
            }
            return res.json(user);
        });
        //Modificar un usuario
        this.updateUser = (req, res) => __awaiter(this, void 0, void 0, function* () {
            //Actualizar usuario sin cambiar password
            const updateUserBase = () => __awaiter(this, void 0, void 0, function* () {
                const updatedUser = yield user_model_1.default.findByIdAndUpdate(userId, {
                    // busco y actualizo en la bd
                    email,
                    name,
                    username,
                    role,
                }, { new: true }); // para que no me devuelva el usuario anterior sino el nuevo modificado
                return res.json({
                    message: "Usuario Modificado",
                    updatedUser,
                });
            });
            //Actualizar usuario con nueva password
            const updateUserPassword = () => __awaiter(this, void 0, void 0, function* () {
                const newPassword = yield encryptPassword_1.default(password);
                password = newPassword;
                console.log(newPassword);
                const updatedUser = yield user_model_1.default.findByIdAndUpdate(userId, {
                    // busco y actualizo en la bd
                    email,
                    name,
                    username,
                    password,
                    role,
                }, { new: true }); // para que no me devuelva el usuario anterior sino el nuevo modificado
                return res.json({
                    message: "Usuario Modificado",
                    updatedUser,
                });
            });
            const { userId } = req.params;
            const userSearched = yield user_model_1.default.findById(userId);
            let { email, name, username, password, role } = req.body;
            if (!username || !password || !name || !role || !email) {
                // Verifico si me mandan todos los datos
                return res.status(400).json({ msg: "Debe Ingresar todos los datos" });
            }
            const isMatchPassword = yield (userSearched === null || userSearched === void 0 ? void 0 : userSearched.comparePassword(password));
            if (isMatchPassword) {
                updateUserBase();
            }
            updateUserPassword();
        });
        //Eliminar un usuario
        this.deleteUser = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { userId } = req.params;
            const user = yield user_model_1.default.findByIdAndRemove(userId);
            if (user && user.imagePath.length > 0) {
                yield fs_extra_1.default.unlink(path_1.default.resolve(user.imagePath)); // le paso la direccion de la imagen a traves de path
            }
            return res.json({
                message: "Usuario eliminado",
                user,
            });
        });
    }
}
exports.UserController = UserController;
