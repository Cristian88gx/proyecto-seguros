"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.changePassword = exports.signOut = exports.infoProfile = exports.signIn = void 0;
const user_model_1 = __importDefault(require("../models/user.model"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const config_1 = __importDefault(require("../config/config"));
const encryptPassword_1 = __importDefault(require("../utils/encryptPassword"));
// Crear token
const createToken = (user) => {
    return jsonwebtoken_1.default.sign({ id: user.id, username: user.username, role: user.role }, config_1.default.jwtSecret, {
        expiresIn: 86400,
    });
};
// Login
exports.signIn = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    if (!req.body.username || !req.body.password) {
        // Verifico si el usuario me manda usuario y password
        return res.status(400).json({ msg: "Debe Ingresar su usuario y password" });
    }
    const user = yield user_model_1.default.findOne({ username: req.body.username }); // Busco en la bd si ya esta registrado el email
    if (!user) {
        return res.status(400).json({ msg: "El usuario no existe" });
    }
    const isMatch = yield user.comparePassword(req.body.password); // comparo el password ingresado
    if (isMatch) {
        return res.status(200).json({
            username: user.username,
            role: user.role,
            email: user.email,
            token: createToken(user),
            expiresIn: 168,
            message: "Login correcto!",
            success: true,
        }); // devuelvo el token
    }
    return res.status(400).json({
        // si el password es incorrecto devuelvo un msje de error
        msg: "Password incorrecto",
    });
});
// Informacion del perfil
exports.infoProfile = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const user = yield user_model_1.default.findOne({ username: req.body.username });
    if (!user) {
        // controlo que el usuario este en la bd
        return res.status(404).send("No user found");
    }
    res.json({
        name: user.name,
        username: user.username,
        email: user.email,
        role: user.role,
    });
});
//Desloguearse
exports.signOut = (req, res) => {
    req.logout();
    res.status(200).send("Logout Exitoso");
};
// Cambiar password (user)
exports.changePassword = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { username } = res.locals.jwtPayload;
    const { oldPassword, newPassword } = req.body;
    if (!oldPassword || !newPassword) {
        res
            .status(400)
            .json({ message: "Password actual y el nuevo son requeridos" });
    }
    const user = yield user_model_1.default.findOne(username); // busco en la bd
    if (!user) {
        return res.status(404).json("usuario inexistente");
    }
    if (!user.comparePassword(oldPassword)) {
        return res.status(404).json("password incorrecto");
    }
    user.password = yield encryptPassword_1.default(newPassword);
    yield user.save();
    return res.status(200).json("password actualizada!");
});
