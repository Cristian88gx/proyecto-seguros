"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.VehicleController = void 0;
const vehicle_model_1 = __importDefault(require("../models/vehicle.model"));
const insured_model_1 = __importDefault(require("../models/insured.model"));
class VehicleController {
    constructor() {
        //Agregar vehiculo
        this.addNewVehicle = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const { brand, model_vehicle, year, domain, num_chasis, num_motor, type_vehicle, uses, color, holder, observation, } = req.body;
                const { dniInsured } = req.params;
                if (!brand ||
                    !model_vehicle ||
                    !year ||
                    !domain ||
                    !num_chasis ||
                    !num_motor ||
                    !type_vehicle ||
                    !uses ||
                    !color ||
                    !holder ||
                    !observation) {
                    // Verifico si me mandan los campos obligatorios
                    return res.status(400).json({ msg: "Debe Ingresar todos los datos" });
                }
                const vehicle = yield vehicle_model_1.default.findOne({ domain: req.body.domain }); // Busco en la bd si ya esta registrado el vehiculo
                if (vehicle) {
                    //Verifico que el vehiculo no este registrado ya
                    return res.status(404).json({ msg: "El vehiculo ya esta registrado" });
                }
                if (!dniInsured)
                    return res.status(404).json({ msg: "Debe pasar un asegurado" });
                const insured = yield insured_model_1.default.findOne({ dni: dniInsured });
                if (insured) {
                    const newVehicle = new vehicle_model_1.default({
                        brand: brand,
                        model_vehicle: model_vehicle,
                        year: year,
                        domain: domain,
                        num_chasis: num_chasis,
                        num_motor: num_motor,
                        type_vehicle: type_vehicle,
                        uses: uses,
                        color: color,
                        holder: holder,
                        observation: observation,
                        insured: insured._id,
                    });
                    yield newVehicle.save();
                    return res.status(201).json({
                        // devuelvo el status y la info del nuevo vehiculo
                        brand: newVehicle.brand,
                        model_vehicle: newVehicle.model_vehicle,
                        year: newVehicle.year,
                        domain: newVehicle.domain,
                        num_chasis: newVehicle.num_chasis,
                        num_motor: newVehicle.num_motor,
                        type_vehicle: newVehicle.type_vehicle,
                        uses: newVehicle.uses,
                        color: newVehicle.color,
                        holder: newVehicle.holder,
                        observation: newVehicle.observation,
                        insured: newVehicle.insured,
                        message: "Vehiculo Creado!",
                        success: true,
                    });
                }
                return res.status(404).json({ msg: "No se encontro un asegurado" });
            }
            catch (err) {
                console.log(err);
                return res.status(500).json({
                    message: "Error creando el vehiculo",
                    success: false,
                });
            }
        });
        //Traer todos los vehiculos
        this.getVehicles = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const vehicles = yield vehicle_model_1.default.find().sort('brand').populate('insured', 'email name surname dni date_of_birth adress city'); // guardo en un arreglo los vehiculos de la bd
                return res.json(vehicles);
            }
            catch (err) {
                console.log(err);
                return res.status(500).json({
                    message: "Error consultando los vehiculos",
                    success: false,
                });
            }
        });
        //Consultar un vehiculo por dominio
        this.getVehicleWithDomain = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const { vehicleDomain } = req.params; // guardo el dominio en una constante
                const vehicle = yield vehicle_model_1.default.findOne({ domain: vehicleDomain }); // busco en la bd
                if (!vehicle) {
                    return res.status(404).json("No se encontro el vehiculo");
                }
                return res.status(201).json(vehicle);
            }
            catch (err) {
                console.log(err);
                return res.status(500).json({
                    message: "Error consultando el vehiculo",
                    success: false,
                });
            }
        });
        //Consultar vehiculos por asegurado
        this.getVehiclesWithInsured = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const { dniInsured } = req.params;
                const insured = yield vehicle_model_1.default.findMyInsured(dniInsured);
                if (!insured) {
                    return res.status(500).json({
                        message: "No se encontro asegurado.",
                        success: false,
                    });
                }
                const vehicles = yield vehicle_model_1.default.find({ insured: insured._id });
                if (!vehicles) {
                    return res.status(500).json({
                        message: "No tiene vehiculos el asegurado.",
                        success: false,
                    });
                }
                // guardo en un arreglo el o los asegurados que coincidan con el apellido
                return res.json(vehicles);
            }
            catch (err) {
                console.log(err);
                return res.status(500).json({
                    message: "Error buscando vehiculos por asegurado.",
                    success: false,
                });
            }
        });
        //Consultar vehiculos por titular
        this.getVehiclesWithHolder = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const { holder } = req.params;
                const vehicle = yield vehicle_model_1.default.findOne({ holder: holder }); // guardo en un arreglo el o los asegurados que coincidan con el apellido
                if (!vehicle) {
                    return res.status(400).json({ message: "No se encontro vehiculo" });
                }
                return res.json(vehicle);
            }
            catch (err) {
                console.log(err);
                return res.status(500).json({
                    message: "Error buscando vehiculos",
                    success: false,
                });
            }
        });
        //Consultar vehiculos por marca
        this.getVehiclesWithBrand = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const { brand } = req.params;
                if (!brand)
                    return res.status(400).json({
                        message: "Debe pasar una marca",
                    });
                const vehicles = yield vehicle_model_1.default.find({ brand: brand }); // guardo en un arreglo el o los asegurados que coincidan con el apellido
                if (!vehicles) {
                    return res.status(400).json({
                        message: "No se encontro vehiculos",
                    });
                }
                return res.status(200).json(vehicles);
            }
            catch (err) {
                console.log(err);
                return res.status(500).json({
                    message: "Error buscando asegurados por fecha de creacion",
                    success: false,
                });
            }
        });
        //Modificar un vehiculo (completo)
        this.updateVehicle = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const { vehicleId } = req.params;
                const { brand, model_vehicle, year, domain, num_chasis, num_motor, type_vehicle, uses, color, holder, observation, insured, } = req.body;
                if (!brand ||
                    !model_vehicle ||
                    !year ||
                    !domain ||
                    !num_chasis ||
                    !num_motor ||
                    !type_vehicle ||
                    !uses ||
                    !color ||
                    !holder ||
                    !observation ||
                    !insured) {
                    // Verifico si me mandan los campos obligatorios
                    return res.status(400).json({ msg: "Debe completar todos los campos" });
                }
                const updatedVehicle = yield vehicle_model_1.default.findByIdAndUpdate(vehicleId, {
                    // busco y actualizo en la bd
                    brand,
                    model_vehicle,
                    year,
                    domain,
                    num_chasis,
                    num_motor,
                    type_vehicle,
                    uses,
                    color,
                    holder,
                    observation,
                    insured,
                }, { new: true }); // para que no me devuelva el asegurado anterior sino el nuevo modificado
                return res.status(200).json({
                    message: "Datos Modificados",
                    updatedVehicle,
                });
            }
            catch (err) {
                console.log(err);
                return res.status(500).json({
                    message: "Error actualizando el vehiculo.",
                    success: false,
                });
            }
        });
        //Consultar vehiculos por fecha de creacion
        this.getVehiclesWithDateCreated = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                if (req.params == null) {
                    return res.status(400).json({
                        message: "Debe pasar una fecha",
                    });
                }
                const { date } = req.params;
                const fechaSig = new Date(date);
                const fecha = new Date(date);
                fechaSig.setDate(fecha.getDate() + 1);
                const vehicles = yield vehicle_model_1.default.find({
                    createdAt: { $gte: fecha, $lt: fechaSig },
                });
                if (vehicles.length === 0) {
                    return res.status(400).json({
                        message: "No hay vehiculos",
                    });
                }
                return res.status(200).json(vehicles);
            }
            catch (err) {
                console.log(err);
                return res.status(500).json({
                    message: "Error buscando vehiculos por fecha de creacion",
                    success: false,
                });
            }
        });
        //Modificar un vehiculo (basico)
        this.updateVehicleBasic = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const { vehicleId } = req.params;
                const { brand, model_vehicle, year, domain, num_chasis, num_motor, type_vehicle, uses, color, holder, observation, } = req.body;
                if (!brand ||
                    !model_vehicle ||
                    !year ||
                    !domain ||
                    !num_chasis ||
                    !num_motor ||
                    !type_vehicle ||
                    !uses ||
                    !color ||
                    !holder) {
                    // Verifico si me mandan los campos obligatorios
                    return res.status(400).json({ msg: "Debe completar todos los campos" });
                }
                const updatedVehicle = yield vehicle_model_1.default.findByIdAndUpdate(vehicleId, {
                    // busco y actualizo en la bd
                    brand,
                    model_vehicle,
                    year,
                    domain,
                    num_chasis,
                    num_motor,
                    type_vehicle,
                    uses,
                    color,
                    holder,
                    observation,
                }, { new: true }); // para que no me devuelva el asegurado anterior sino el nuevo modificado
                return res.status(200).json({
                    message: "Datos Modificados",
                    updatedVehicle,
                });
            }
            catch (err) {
                console.log(err);
                return res.status(500).json({
                    message: "Error actualizando el vehiculo.",
                    success: false,
                });
            }
        });
        //Eliminar un vehiculo
        this.deleteVehicle = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const { vehicleId } = req.params;
                const vehicle = yield vehicle_model_1.default.findByIdAndRemove(vehicleId);
                if (vehicle) {
                    return res.status(200).json({
                        message: "Asegurado eliminado",
                        vehicle,
                    });
                }
            }
            catch (err) {
                console.log(err);
                return res.status(500).json({
                    message: "Error eliminando el asegurado.",
                    success: false,
                });
            }
        });
    }
}
exports.VehicleController = VehicleController;
