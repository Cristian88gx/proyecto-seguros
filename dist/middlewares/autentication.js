"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const config_1 = __importDefault(require("../config/config"));
function verificarToken(req, res, next) {
    const authorization = req.headers.authorization;
    const token = authorization === null || authorization === void 0 ? void 0 : authorization.slice(7, authorization.length); // extraigo el token
    jsonwebtoken_1.default.verify(String(token), config_1.default.jwtSecret, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                ok: false,
                err
            });
        }
        req.user = decoded.user;
        next();
    });
}
exports.default = verificarToken;
