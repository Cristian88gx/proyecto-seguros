"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jwt_decode_1 = __importDefault(require("jwt-decode"));
function checkRole(roles) {
    return (req, res, next) => __awaiter(this, void 0, void 0, function* () {
        const authorization = req.headers.authorization;
        const token = authorization === null || authorization === void 0 ? void 0 : authorization.slice(7, authorization.length); // extraigo el token
        const decoded = JSON.stringify(jwt_decode_1.default(String(token))); // lo decodifico
        const decodedJson = JSON.parse(decoded);
        const role = decodedJson.role; //extraigo el rol del payload
        // checkeo el role
        if (!role) {
            return res.status(401).send({ message: "No autorizado" });
        }
        if (roles.includes(role)) {
            next(); // continuo normal
        }
        else {
            return res.status(401).send({ message: "No autorizado" });
        }
    });
}
exports.default = checkRole;
