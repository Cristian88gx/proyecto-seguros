import multer from "multer"; // es un modulo que me permite guardar imagenes
import { v4 as uuidv4 } from 'uuid'; // es un modulo que devuelve una cadena aleatoria
import path from "path"; // maneja la ubicacion de los archivos y sus extensiones

const storage = multer.diskStorage({
    destination: 'uploads', // donde gardar las imagenes
    filename: (req, file, cb) => {
        cb(null, uuidv4() + path.extname(file.originalname)); // le asigno un nombre aleatorio a la imagen y su extension
    }
});

export default multer({storage});