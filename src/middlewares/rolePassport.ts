import { Strategy, ExtractJwt, StrategyOptions } from "passport-jwt";
import config from "../config/config";

// Opciones de la estragia de validacion
const opts: StrategyOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: config.jwtSecret,
};
// Estrategia de validacion
export default new Strategy(opts, async (payload, done) => {
  try {
    const role = payload.role; // extraigo el rol del payload del token
    if (role) {
      return done(null, role); // si existe devuelvo el role
    }
    return done(null, false); // sino devuelvo false
  } catch (error) {
    console.log(error);
  }
});