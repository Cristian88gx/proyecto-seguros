import jwt from 'jsonwebtoken';
import config from "../config/config";
import { NextFunction,Request, Response } from 'express'

export default function verificarToken(req: Request,res: Response,next: NextFunction) {
    const authorization = req.headers.authorization;
    const token = authorization?.slice(7, authorization.length); // extraigo el token
    jwt.verify(String(token),config.jwtSecret,(err: any, decoded: any)=>{
        if (err) {
            return res.status(401).json({
                ok:false,
                err
            })
        }
        req.user = decoded.user;
        next();
    })
}