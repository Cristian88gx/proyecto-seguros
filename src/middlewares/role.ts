import { Request, Response, NextFunction } from "express";
import jwtDecode from 'jwt-decode';

export default function checkRole(roles: Array<string>) {
  return async (req: Request, res: Response, next: NextFunction) => {
    const authorization = req.headers.authorization;
    const token = authorization?.slice(7, authorization.length); // extraigo el token
    const decoded = JSON.stringify(jwtDecode(String(token))); // lo decodifico
    const decodedJson = JSON.parse(decoded);
    const role = decodedJson.role; //extraigo el rol del payload
    // checkeo el role
    if (!role) {
      return res.status(401).send({ message: "No autorizado" });
    }
    if (roles.includes(role)) {
      next(); // continuo normal
    } else {
      return res.status(401).send({ message: "No autorizado" });
    }
  };
}
