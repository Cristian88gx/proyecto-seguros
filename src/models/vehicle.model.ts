import { Schema, model, Document, Model } from "mongoose";
import { IInsured,insuredSchema } from "./insured.model";

interface IVehicleSchema extends Document {
  brand: string;
  model_vehicle: string;
  year: number;
  domain: string;
  num_chasis: string;
  num_motor: string;
  type_vehicle: string;
  uses: string;
  color: string;
  holder: string;
  observation: string;
}

export interface IVehicle extends IVehicleSchema {
  insured: IInsured['_id'];
}

export interface IVehicleModel extends Model<IVehicle> {
  findMyInsured(id: string): Promise<IInsured>
}
                                                       
export const vehicleSchema = new Schema(
  {
    brand: {
      type: String,
      required: true,
      uppercase: true,
      trim: true,
    },
    model_vehicle: {
      type: String,
      required: true,
      uppercase: true,
      trim: true,
    },
    year: {
      type: Number,
      required: true,
      trim: true,
    },
    domain: {
      type: String,
      unique: true,
      required: true,
      uppercase: true,
      trim: true,
    },
    num_chasis: {
      type: String,
      unique: true,
      uppercase: true,
      required: true,
      trim: true,
    },
    num_motor: {
      type: String,
      unique: true,
      uppercase: true,
      required: true,
      trim: true,
    },
    type_vehicle: {
      type: String,
      required: true,
      uppercase: true,
      trim: true,
    },
    uses: {
      type: String,
      required: true,
      uppercase: true,
      trim: true,
    },
    color: {
      type: String,
      required: true,
      uppercase: true,
      trim: true,
    },
    holder: {
      type: String,
      required: true,
      uppercase: true,
      trim: true,
    },
    observation: {
      type: String,
      required: false,
      default: "",
      uppercase: true,
      trim: true,
    },
    insured: {
      type: Schema.Types.ObjectId,
      ref: "Insured",
      required: true,
    },
  },
  { timestamps: true }
);

// Static methods
vehicleSchema.statics.findMyInsured = function (dni: string) {
  return this.findOne(dni).populate("Insured").exec();
};

export default model<IVehicle, IVehicleModel>("Vehicle", vehicleSchema);