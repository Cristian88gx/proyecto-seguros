import { model, Schema, Document } from "mongoose";
import bcrypt from "bcrypt";

export interface IUser extends Document {
  email: string;
  name: string;
  username: string;
  password: string;
  role: string;
  imagePath: string;
  comparePassword: (password: string) => Promise<Boolean>;
}

const userSchema = new Schema(
  {
    email: {
      type: String,
      unique: true,
      required: true,
      lowercase: true,
      trim: true,
    },
    name: {
      type: String,
      required: true,
      trim: true,
    },
    username: {
      type: String,
      unique: true,
      required: true,
      trim: true,
    },
    password: {
      type: String,
      required: true,
    },
    role: {
      type: String,
      default: "user",
      enum: ["user", "admin", "superadmin"],
    },
    imagePath: {
      type: String,
      default: '',
      required: false,
    },
  },
  { timestamps: true }
);

// Metodo para cifrar el password antes de guardar

userSchema.pre<IUser>("save", async function (next) {
  const user = this;
  if (!user.isModified("password")) return next(); // si no se modifico el password sigue adelante
  const salt = await bcrypt.genSalt(10);
  const hash = await bcrypt.hash(user.password, salt);
  user.password = hash; // sino se cifra el nuevo password
  next();
});

userSchema.pre<IUser>("findByIdAndUpdate", async function (next) {
  const user = this;
  const salt = await bcrypt.genSalt(10);
  const hash = await bcrypt.hash(user.password, salt);
  user.password = hash; // sino se cifra el nuevo password
  next();
});

// Metodo para cifrar el password al actualizar

userSchema.methods.encryptPassword = async (password: String) => {
  const salt = await bcrypt.genSalt(10);
  return bcrypt.hash(password, salt); //devuelve el password encriptado
};

// Metodo para comparar el password

userSchema.methods.comparePassword = async function (
  password: string
): Promise<Boolean> {
  return await bcrypt.compare(password, this.password);
};

export default model<IUser>("User", userSchema);
