import { Schema, model, Document, Types } from "mongoose";
import { IPolicy } from "./policy.model";
import {ICuote} from "./cuote.model";

export interface IOperation extends Document {
  date_of_payment: Date;
  hour_of_payment: Date;
  amount: number;
  policy_id: IPolicy["_id"];
  cuotes: Types.Array<ICuote>;
}

const operationSchema = new Schema(
  {
    date_of_payment: {
      type: Date,
      required: true,
    },
    hour_of_expiration: {
      type: Date,
      required: true,
    },
    amount: {
      type: Number,
      required: true
    },
    policy_id: {
        type: Schema.Types.ObjectId,
        ref: "Policy",
        required: true
    },
    cuotes: [
        {
            type: Schema.Types.ObjectId,
            ref: "Policy",
        },
    ]
  },
  { timestamps: true }
);

export default model<IOperation>("Operation", operationSchema);