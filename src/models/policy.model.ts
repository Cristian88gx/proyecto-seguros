import { Schema, model, Document, Types, Model } from "mongoose";
import { IVehicle } from "./vehicle.model";
import { ICuote } from "./cuote.model";
import { IOffice } from "./office.model";
import { IInsured} from "./insured.model";

export interface IPolicy extends Document {
  num_policy: string;
  num_proposal: string;
  section: string;
  date_init: Date;
  date_finish: Date;
  date_reception: Date;
  date_high: Date;
  num_cuotes: number;
  amount_total: number;
  state: string;
  observation: string;
  vehicle: IVehicle['_id'];
  insured: IInsured['_id']
  office: IOffice['_id'];
  cuotes: Types.Array<ICuote>;
  findMyInsured(id: String): Promise<IVehicle>;
}

const policySchema = new Schema(
  {
    num_policy: {
      type: String,
      required: true,
      unique: true,
      uppercase: true,
      trim: true,
    },
    num_proposal: {
      type: String,
      required: true,
      unique: true,
      uppercase: true,
      trim: true,
    },
    section: {
      type: Number,
      required: true,
      trim: true,
    },
    date_init: {
      type: Date,
      required: true,
      default: Date.now,
      min: Date.now,
    },
    date_finish: {
      type: Date,
      required: true,
      default: Date.now,
    },
    date_reception: {
      type: Date,
      default: Date.now,
    },
    date_hight: {
      type: Date,
      required: false,
      default: Date.now,
    },
    num_cuotes: {
      type: Number,
      required: true,
      trim: true,
    },
    amount_total: {
      type: Number,
      required: true,
      trim: true,
    },
    state: {
      type: String,
      required: true,
      default: "en tramite",
      uppercase: true,
      trim: true,
    },
    observation: {
      type: String,
      required: true,
      uppercase: true,
      trim: true,
    },
    vehicle: {
      type: Schema.Types.ObjectId,
      ref: "Vehicle",
      required: true,
    },
    insured: {
      type: Schema.Types.ObjectId,
      ref: "Insured",
      required: true,
    },
    office: {
      type: Schema.Types.ObjectId,
      ref: "Office",
      required: true,
    },
    cuotes: [
      {
        type: Schema.Types.ObjectId,
        ref: "Cuote",
        default: [],
        required: false,
      },
    ],
  },
  { timestamps: true }
);

// Static methods
policySchema.statics.findMyVehicle = async function (id: String) {
  return this.findById(id).populate("Vehicle").exec();
};

policySchema.statics.findMyOffice = async function (id: String) {
  return this.findById(id).populate("Office").exec();
};

// For model
export interface IPolicyExtend extends Model<IPolicy> {
  findMyInsured(id: String): Promise<IVehicle>;
}

export default model<IPolicy>("Policy", policySchema);