import { Schema, model, Document} from "mongoose";

export interface IInsured extends Document {
  name: string;
  surname: string;
  dni: string;
  date_of_birth: Date;
  gender: string;
  adress: string;
  city: string;
  email: string;
  phone: string;
  civil_state: string;
  occupation: string;
}

export const insuredSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
      uppercase: true,
      trim: true,
    },
    surname: {
      type: String,
      required: true,
      uppercase: true,
      trim: true,
    },
    dni: {
      type: String,
      unique: true,
      required: true,
      uppercase: true,
      trim: true,
    },
    date_of_birth: {
      type: Date,
      required: true,
      default: Date.now,
      min: "1900-01-01",
      max: Date.now,
    },
    gender: {
      type: String,
      required: true,
      uppercase: true,
      trim: true,
    },
    adress: {
      type: String,
      required: true,
      trim: true
    },
    city: {
      type: String,
      required: true,
      uppercase: true,
      trim: true
    },
    email: {
      type: String,
      default: '',
      required: false,
      uppercase: false,
      trim: true
    },
    phone: {
      type: String,
      required: true,
      uppercase: false,
      trim: true,
    },
    civil_state: {
      type: String,
      required: true,
      uppercase: true,
      trim: true,
    },
    occupation: {
      type: String,
      required: true,
      uppercase: true,
      trim: true,
    },
  },
  { timestamps: true }
);

// Campos Virtuales
insuredSchema.virtual("fullName").get(function (name: string, surname: string) {
  return name + surname;
});

export default model<IInsured>("Insured", insuredSchema);