import {Schema, model, Document} from "mongoose";

export interface ICompany extends Document {
  name: string;
  phone: string,
  adress: string;
  email: string;
}

export const companySchema = new Schema(
    {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    adress: {
      type: String,
      required: true,
      trim: true,
    },
    phone: {
        type: String,
        required: true,
        trim: true,
    },
    email: {
      type: String,
      required: true,
      trim: true,
    },
  },
  { timestamps: true }
);

export default model<ICompany>("Company", companySchema);