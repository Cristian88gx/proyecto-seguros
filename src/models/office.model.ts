import {Schema, model, Document} from "mongoose";

export interface IOffice extends Document {
  name: string;
  adress: string;
  email: string;
}

export const officeSchema = new Schema(
    {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    adress: {
      type: String,
      required: true,
      trim: true,
    },
    email: {
      type: String,
      required: true,
      trim: true,
    },
  },
  { timestamps: true }
);

export default model<IOffice>("Office", officeSchema);