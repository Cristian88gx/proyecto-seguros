import { Schema, model, Document, Types } from "mongoose";
import { IPolicy } from "./policy.model";

export interface ICuote extends Document {
  import: number;
  date_of_expiration: Date;
  paid_out: boolean;
  policy_id: IPolicy["_id"];
}

const cuoteSchema = new Schema(
  {
    import: {
      type: Number,
      required: true,
    },
    date_of_expiration: {
      type: Date,
      required: true,
      min: "1900-01-01",
    },
    paid_out: {
      type: Boolean,
      required: true,
      default: false,
    },
    policy_id: {
        type: Schema.Types.ObjectId,
        ref: "Policy",
        required: true
    },
  },
  { timestamps: true }
);

export default model<ICuote>("Cuote", cuoteSchema);