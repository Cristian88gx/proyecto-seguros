import { Request, Response } from "express";
import User, { IUser } from "../models/user.model";
import jwt from "jsonwebtoken";
import config from "../config/config";
import encryptPassword from "../utils/encryptPassword";

// Crear token
const createToken = (user: IUser) => {
  return jwt.sign(
    { id: user.id, username: user.username, role: user.role },
    config.jwtSecret,
    {
      expiresIn: 86400, // tiempo de expiracion del token
    }
  );
}
// Login
export const signIn = async (
  req: Request,
  res: Response
): Promise<Response> => {
  if (!req.body.username || !req.body.password) {
    // Verifico si el usuario me manda usuario y password
    return res.status(400).json({ msg: "Debe Ingresar su usuario y password" });
  }
  const user = await User.findOne({ username: req.body.username }); // Busco en la bd si ya esta registrado el email
  if (!user) {
    return res.status(400).json({ msg: "El usuario no existe" });
  }
  const isMatch = await user.comparePassword(req.body.password); // comparo el password ingresado
  if (isMatch) {
    return res.status(200).json({
      username: user.username,
      role: user.role,
      email: user.email,
      token: createToken(user),
      expiresIn: 168,
      message: "Login correcto!",
      success: true,
    }); // devuelvo el token
  }
  return res.status(400).json({
    // si el password es incorrecto devuelvo un msje de error
    msg: "Password incorrecto",
  });
};
// Informacion del perfil
export const infoProfile = async (req: Request, res: Response) => {
  const user = await User.findOne({ username: req.body.username });
  if (!user) {
    // controlo que el usuario este en la bd
    return res.status(404).send("No user found");
  }
  res.json({
    name: user.name,
    username: user.username,
    email: user.email,
    role: user.role,
  });
};
//Desloguearse
export const signOut = (req: Request, res: Response) => {
  req.logout();
  res.status(200).send("Logout Exitoso");
};
// Cambiar password (user)
export const changePassword = async (req: Request, res: Response) => {
  const { username } = res.locals.jwtPayload;
  const { oldPassword, newPassword } = req.body;
  if (!oldPassword || !newPassword) {
    res
      .status(400)
      .json({ message: "Password actual y el nuevo son requeridos" });
  }
  const user = await User.findOne(username); // busco en la bd
  if (!user) {
    return res.status(404).json("usuario inexistente");
  }
  if (!user.comparePassword(oldPassword)) {
    return res.status(404).json("password incorrecto");
  }
  user.password = await encryptPassword(newPassword);
  await user.save();
  return res.status(200).json("password actualizada!");
};