import { Request, Response } from "express";
import Insured from "../models/insured.model";

export class InsuredController {
  //Agregar asegurado
  public addNewInsured = async (
    req: Request,
    res: Response
  ): Promise<Response> => {
    try {
      const insuredCreated = new Insured(req.body);
      if (
        !insuredCreated.name ||
        !insuredCreated.surname ||
        !insuredCreated.dni ||
        !insuredCreated.date_of_birth ||
        !insuredCreated.gender ||
        !insuredCreated.adress ||
        !insuredCreated.city ||
        !insuredCreated.phone ||
        !insuredCreated.civil_state ||
        !insuredCreated.occupation
      ) {
        // Verifico si me mandan los campos obligatorios
        return res.status(400).json({ msg: "Debe Ingresar todos los datos" });
      }
      const insured = await Insured.findOne({ dni: req.body.dni }); // Busco en la bd si ya esta registrado el asegurado
      if (insured) {
        //Verifico que el asegurado no este registrado ya
        return res.status(404).json({ msg: "El asegurado ya esta registrado" });
      }
      const newInsured = new Insured({
        name: insuredCreated.name,
        surname: insuredCreated.surname,
        dni: insuredCreated.dni,
        date_of_birth: insuredCreated.date_of_birth,
        gender: insuredCreated.gender,
        adress: insuredCreated.adress,
        email: insuredCreated.email,
        city: insuredCreated.city,
        phone: insuredCreated.phone,
        civil_state: insuredCreated.civil_state,
        occupation: insuredCreated.occupation,
      }); // Creo un nuevo asegurado y le asigno el body del request      await newUser.save();
      await newInsured.save();
      return res.status(201).json({
        // devuelvo el status y la info del nuevo asegurado
        name: newInsured.name,
        surname: newInsured.surname,
        dni: newInsured.dni,
        date_of_birth: newInsured.date_of_birth,
        gender: newInsured.gender,
        adress: newInsured.adress,
        city: newInsured.city,
        phone: newInsured.phone,
        civil_state: newInsured.civil_state,
        occupation: newInsured.occupation,
        email: newInsured.email,
        message: "Registro Completo!",
        success: true,
      });
    } catch (err) {
      console.log(err);
      return res.status(500).json({
        message: "Error creando el asegurado.",
        success: false,
      });
    }
  };
  //Traer todos los asegurados
  public getInsureds = async (
    req: Request,
    res: Response
  ): Promise<Response> => {
    try {
      const insureds = await Insured.find(); // guardo en un arreglo los usuarios de la bd
      return res.json(insureds);
    } catch (err) {
      console.log(err);
      return res.status(500).json({
        message: "Error consultando los asegurados",
        success: false,
      });
    }
  };
  //Consultar un asegurado por dni
  public getInsuredWithDni = async (
    req: Request,
    res: Response
  ): Promise<Response> => {
    try {
      const { insuredDni } = req.params; // guardo el id en una constante
      const insured = await Insured.findOne({ dni: insuredDni }); // busco en la bd
      if (!insured) {
        return res.status(404).json("No se encontro el asegurado");
      }
      return res.json(insured);
    } catch (err) {
      console.log(err);
      return res.status(500).json({
        message: "Error consultando el asegurado",
        success: false,
      });
    }
  };
  //Consultar asegurados por apellido
  public getInsuredsWithSurname = async (
    req: Request,
    res: Response
  ): Promise<Response> => {
    try {
      const { insuredSurname } = req.params;
      const insureds = await Insured.find({ surname: insuredSurname }); // guardo en un arreglo el o los asegurados que coincidan con el apellido
      return res.json(insureds);
    } catch (err) {
      console.log(err);
      return res.status(500).json({
        message: "Error buscando asegurados por apellido.",
        success: false,
      });
    }
  };
  //Consultar asegurados por fecha de creacion
  public getInsuredsWithDateCreated = async (
    req: Request,
    res: Response
  ): Promise<Response> => {
    try {
      const { date } = req.params;
      if (!date)
        return res.status(400).json({
          message: "Debe pasar una fecha",
        });
      const insureds = await Insured.find({ createdAt: date }); // guardo en un arreglo el o los asegurados que coincidan con el apellido
      return res.status(200).json(insureds);
    } catch (err) {
      console.log(err);
      return res.status(500).json({
        message: "Error buscando asegurados por fecha de creacion",
        success: false,
      });
    }
  };
  //Modificar un asegurado
  public updateInsured = async (
    req: Request,
    res: Response
  ): Promise<Response> => {
    try {
      const { insuredId } = req.params;
      const {
        name,
        surname,
        dni,
        date_of_birth,
        gender,
        adress,
        email,
        city,
        phone,
        civil_state,
        occupation,
      } = req.body;
      if (
        !name ||
        !surname ||
        !dni ||
        !date_of_birth ||
        !gender ||
        !adress ||
        !city ||
        !phone ||
        !civil_state ||
        !occupation
      ) {
        // Verifico si me mandan los campos obligatorios
        return res.status(400).json({ msg: "Debe completar todos los campos" });
      }
      const updatedInsured = await Insured.findByIdAndUpdate(
        insuredId,
        {
          // busco y actualizo en la bd
          name,
          surname,
          dni,
          date_of_birth,
          gender,
          adress,
          city,
          email,
          phone,
          civil_state,
          occupation,
        },
        { new: true }
      ); // para que no me devuelva el asegurado anterior sino el nuevo modificado
      return res.status(200).json({
        message: "Datos Modificados",
        updatedInsured,
      });
    } catch (err) {
      console.log(err);
      return res.status(500).json({
        message: "Error actualizando el asegurado.",
        success: false,
      });
    }
  };
  //Eliminar un asegurado
  public deleteInsured = async (
    req: Request,
    res: Response
  ): Promise<Response | undefined> => {
    try {
      const { insuredId } = req.params;
      const insured = await Insured.findByIdAndRemove(insuredId);
      if (insured) {
        return res.status(200).json({
          message: "Asegurado eliminado",
          insured,
        });
      }
    } catch (err) {
      console.log(err);
      return res.status(500).json({
        message: "Error eliminando el asegurado.",
        success: false,
      });
    }
  };
}