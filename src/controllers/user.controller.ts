import { Request, Response } from "express";
import User from "../models/user.model";
import fs from "fs-extra"; // Este modulo permite trabajar con archivos
import path from "path"; // Este modulo maneja las direcciones de los archivos
import encryptPassword from "../utils/encryptPassword";

export class UserController {
  //Agregar usuario
  public addNewUser = async (req: Request, res: Response): Promise<Response> => {
    try {
      const userCreated = new User(req.body);
      console.log("entro");
      if (
        !userCreated.username ||
        !userCreated.password ||
        !userCreated.name ||
        !userCreated.role ||
        !userCreated.email
      ) {
        // Verifico si el usuario me manda usuario y password
        return res.status(400).json({ msg: "Debe Ingresar todos los datos" });
      }
      const user = await User.findOne({ username: req.body.username }); // Busco en la bd si ya esta registrado el usuario
      if (user) {
        //Verifico que el usuario no este registrado ya
        return res.status(404).json({ msg: "El usuario ya esta registrado" });
      }
      if (req.file !== undefined) {
        console.log("entro con foto");
        // Si se proporciona una foto de perfil
        const newUser = new User({
          name: userCreated.name,
          username: userCreated.username,
          email: userCreated.email,
          password: userCreated.password,
          role: userCreated.role,
          imagePath: req.file.path,
        }); // Creo un nuevo usuario y le asigno el body del request
        console.log(newUser);
        await newUser.save();
      }
      console.log("entro sin foto");
      const newUser = new User({
        name: userCreated.name,
        username: userCreated.username,
        email: userCreated.email,
        password: userCreated.password,
        role: userCreated.role,
        imagePath: "",
      }); // Creo un nuevo usuario y le asigno el body del request
      console.log(newUser);
      await newUser.save();

      return res.status(201).json({
        // devuelvo el status y la info del nuevo usuario
        name: newUser.name,
        username: newUser.username,
        role: newUser.role,
        email: newUser.email,
        imagePath: newUser.imagePath,
        message: "Registro Completo!",
        success: true,
      });
    } catch (err) {
      console.log(err);
      return res.status(500).json({
        message: "Error creando el usuario.",
        success: false,
      });
    }
  };
  //Traer todos los usuarios
  public getUsers = async (req: Request, res: Response): Promise<Response> => {
    const users = await User.find(); // guardo en un arreglo los usuarios de la bd
    return res.json(users);
  };
  //Consultar un usuario por id
  public getUserWithId = async (req: Request, res: Response) => {
    const { userId } = req.params; // guardo el id en una constante
    console.log(userId);
    const user = await User.findById(userId); // busco en la bd
    if (!user) {
      return res.status(404).json("No User found");
    }
    return res.json(user);
  };
  //Modificar un usuario
  public updateUser = async (req: Request, res: Response) => {
    //Actualizar usuario sin cambiar password
    const updateUserBase = async () => {
      const updatedUser = await User.findByIdAndUpdate(
        userId,
        {
          // busco y actualizo en la bd
          email,
          name,
          username,
          role,
        },
        { new: true }
      ); // para que no me devuelva el usuario anterior sino el nuevo modificado
      return res.json({
        message: "Usuario Modificado",
        updatedUser,
      });
    };
    //Actualizar usuario con nueva password
    const updateUserPassword = async () => {
      const newPassword = await encryptPassword(password);
      password = newPassword;
      console.log(newPassword);
      const updatedUser = await User.findByIdAndUpdate(
        userId,
        {
          // busco y actualizo en la bd
          email,
          name,
          username,
          password,
          role,
        },
        { new: true }
      ); // para que no me devuelva el usuario anterior sino el nuevo modificado
      return res.json({
        message: "Usuario Modificado",
        updatedUser,
      });
    };
    const { userId } = req.params;
    const userSearched = await User.findById(userId);
    let { email, name, username, password, role } = req.body;
    if (!username || !password || !name || !role || !email) {
      // Verifico si me mandan todos los datos
      return res.status(400).json({ msg: "Debe Ingresar todos los datos" });
    }
    const isMatchPassword = await userSearched?.comparePassword(password);
    if (isMatchPassword) {
      updateUserBase();
    }
    updateUserPassword();
  };
  //Eliminar un usuario
  public deleteUser = async (req: Request, res: Response): Promise<Response> => {
    const { userId } = req.params;
    const user = await User.findByIdAndRemove(userId);
    if (user && user.imagePath.length > 0) {
      await fs.unlink(path.resolve(user.imagePath)); // le paso la direccion de la imagen a traves de path
    }
    return res.json({
      message: "Usuario eliminado",
      user,
    });
  };
}