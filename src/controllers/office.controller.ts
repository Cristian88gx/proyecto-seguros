import { Request, Response } from "express";
import Office from "../models/office.model";

export class OfficeController {
  //Agregar oficina
  public addNewOffice = async (
    req: Request,
    res: Response
  ): Promise<Response> => {
    try {
      const officeCreated = new Office(req.body);
      if (
        !officeCreated.name ||
        !officeCreated.adress ||
        !officeCreated.email
      ) {
        // Verifico si estan todos los datos
        return res.status(400).json({ msg: "Debe Ingresar todos los datos" });
      }
      const office = await Office.findOne({ name: req.body.name }); // Busco en la bd si ya esta registrado
      if (office) {
        //Verifico si ya existe
        return res.status(404).json({ msg: "La oficina ya esta registrada" });
      }
      const newOffice = new Office({
        name: officeCreated.name,
        adress: officeCreated.adress,
        email: officeCreated.email,
      }); // Creo una nueva oficina y le asigno el body del reques
      await newOffice.save();

      return res.status(201).json({
        // devuelvo el status y la info de la nueva oficina
        name: newOffice.name,
        adress: newOffice.adress,
        email: newOffice.email,
        message: "Oficina Creada!",
        success: true,
      });
    } catch (err) {
      console.log(err);
      return res.status(500).json({
        message: "Error creando la oficina.",
        success: false,
      });
    }
  };
  //Traer todas las oficinas
  public getOffices = async (
    req: Request,
    res: Response
  ): Promise<Response> => {
    const offices = await Office.find(); // guardo en un arreglo las oficinas de la bd
    return res.json(offices);
  };
  //Consultar una oficina por id
  public getOfficeWithId = async (req: Request, res: Response) => {
    const { officeId } = req.params; // guardo el id en una constante
    const office = await Office.findById(officeId); // busco en la bd
    if (!office) {
      return res.status(404).json("No Office found");
    }
    return res.json(office);
  };
  //Modificar una oficina
  public updateOffice = async (
    req: Request,
    res: Response
  ): Promise<Response> => {
    try {
      const { officeId } = req.params;
      const { name, adress, email } = req.body;
      if (!name || !adress || !email) {
        // Verifico si me mandan los campos obligatorios
        return res.status(400).json({ msg: "Debe completar todos los campos" });
      }
      const updatedOffice = await Office.findByIdAndUpdate(
        officeId,
        {
          // busco y actualizo en la bd
          name,
          adress,
          email,
        },
        { new: true }
      ); // para que no me devuelva la oficina anterior sino la nueva modificada
      return res.status(200).json({
        message: "Datos Modificados",
        updatedOffice,
      });
    } catch (err) {
      console.log(err);
      return res.status(500).json({
        message: "Error actualizando la oficina.",
        success: false,
      });
    }
  };
  //Eliminar una oficina
  public deleteOffice = async (
    req: Request,
    res: Response
  ): Promise<Response> => {
    try {
      const { officeId } = req.params;
      const office = await Office.findByIdAndRemove(officeId);
      if (!officeId || !office) {
        return res.status(400).json({
          message: "Debe seleccionar una oficina",
        });
      }
      return res.status(200).json({
        message: "Oficina eliminada",
        office,
      });
    } catch (err) {
      console.log(err);
      return res.status(500).json({
        message: "Error eliminando la oficina.",
        success: false,
      });
    }
  };
}