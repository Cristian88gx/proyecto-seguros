import bcrypt from "bcrypt";
const encryptPassword = async (password: String) => {
    const salt = await bcrypt.genSalt(10);
    return bcrypt.hash(password, salt); //devuelve el password encriptado
};

export default encryptPassword;