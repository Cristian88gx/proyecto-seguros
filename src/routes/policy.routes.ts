import { Router } from "express";
import passport from "passport";
import checkRole from "../middlewares/role";
import { PolicyController } from "../controllers/policy.controller";

const policyController: PolicyController = new PolicyController();
const router = Router();
const autenticateJwt = passport.authenticate("jwt", { session: false });

// Crear una poliza nueva
router.post(
  "/createPolicy/:idVehicle/:idOffice",
  [autenticateJwt, checkRole(["admin", "superadmin"])],
  policyController.addNewPolicy
);
// Obtener todas las polizas
router.get(
  "/getPolicies",
  [autenticateJwt, checkRole(["admin", "superadmin"])],
  policyController.getPolicies
);
// Buscar una poliza por su id
router.get(
  "/searchPolicy/:policyId",
  [autenticateJwt, checkRole(["admin", "superadmin"])],
  //policyController.getOfficeWithId
);
// Modificar una poliza
router.put(
  "/updateOffice/:officeId",
  [autenticateJwt, checkRole(["admin", "superadmin"])],
  //policyController.updateOffice
);
// Eliminar una poliza por su id
router.delete(
  "/deletePolicy/:policyId",
  [autenticateJwt, checkRole(["admin", "superadmin"])],
  //policyController.deletePolicy
);

export default router;