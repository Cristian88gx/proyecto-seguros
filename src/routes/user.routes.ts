import { Router } from "express";
import passport from "passport";
import checkRole from "../middlewares/role";
import multer from "../libs/multer";
import { UserController } from "../controllers/user.controller";

const userController: UserController = new UserController();
const router = Router();
const autenticateJwt = passport.authenticate("jwt", { session: false });

// Crear un usuario nuevo
router.post(
  "/createUser",
  
  userController.addNewUser
);
// Obtener todos los usuarios
router.get(
  "/getUsers",
 
  userController.getUsers
);
// Buscar un usuario por su id
router.get(
  "/searchUser/:userId",
  
  userController.getUserWithId
);
// Modificar un usuario
router.put(
  "/updateUser/:userId",
  
  userController.updateUser
);
// Eliminar un usuario por su id
router.delete(
  "/deleteUser/:userId",
 
  userController.deleteUser
);

export default router;