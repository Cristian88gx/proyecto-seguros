import { Router } from "express";
import passport from "passport";
import checkRole from "../middlewares/role";
import { OfficeController } from "../controllers/office.controller";

const officeController: OfficeController = new OfficeController();
const router = Router();
const autenticateJwt = passport.authenticate("jwt", { session: false });

// Crear una oficina nueva
router.post(
  "/createOffice",
  //[autenticateJwt, checkRole(["admin", "superadmin"])],
  officeController.addNewOffice
);
// Obtener todas las oficinas
router.get(
  "/getOffices",
  //[autenticateJwt, checkRole(["admin", "superadmin"])],
  officeController.getOffices
);
// Buscar una oficina por su id
router.get(
  "/searchOffice/:officeId",
  [autenticateJwt, checkRole(["admin", "superadmin"])],
  officeController.getOfficeWithId
);
// Modificar una oficina
router.put(
  "/updateOffice/:officeId",
  [autenticateJwt, checkRole(["admin", "superadmin"])],
  officeController.updateOffice
);
// Eliminar una oficina por su id
router.delete(
  "/deleteOffice/:officeId",
  [autenticateJwt, checkRole(["admin", "superadmin"])],
  officeController.deleteOffice
);

export default router;