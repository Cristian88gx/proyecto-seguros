import { Router } from "express";
import passport from "passport";
import { signIn, signOut, infoProfile } from "../controllers/auth.controller";

const router = Router();
const authenticateJwt = passport.authenticate("jwt", { session: false });

// Login
router.post("/signIn", signIn);
// Logout
router.post("/signOut",  signOut);
// Info Profile
router.get("/profile", authenticateJwt, infoProfile);

export default router;