import { Router } from "express";
import passport from "passport";
import checkRole from "../middlewares/role";
import { VehicleController } from "../controllers/vehicle.controller";

const vehicleController: VehicleController = new VehicleController();
const router = Router();
const autenticateJwt = passport.authenticate("jwt", { session: false });

// Crear un vehiculo nuevo
router.post(
  "/createVehicle/:dniInsured",
  
  vehicleController.addNewVehicle
);
// Obtener todos los vehiculos
router.get(
  "/getVehicles",
 
  vehicleController.getVehicles
);
// Buscar un vehiculo por su dominio
router.get(
  "/searchVehicle/:vehicleDomain",
  [autenticateJwt, checkRole(["admin", "user", "superadmin"])],
  vehicleController.getVehicleWithDomain
);
// Buscar vehiculos de un asegurado
router.get(
  "/searchVehicles/:IdInsured",
 
  vehicleController.getVehiclesWithInsured
);
// Buscar un vehiculo por su titular
router.get(
    "/searchVehicle/:vehicleHolder",
    [autenticateJwt, checkRole(["admin", "user", "superadmin"])],
    vehicleController.getVehiclesWithHolder
  );
// Buscar vehiculos por fecha de creacion
router.get(
  "/searchVehiclesCreated/:date",
  [autenticateJwt, checkRole(["admin", "superadmin"])],
  vehicleController.getVehiclesWithDateCreated
);
// Buscar vehiculos por Marca
router.get(
  "/searchVehiclesBrand/:brand",
  [autenticateJwt, checkRole(["admin", "superadmin"])],
  vehicleController.getVehiclesWithBrand
);
// Modificar un vehiculo (basico)
router.put(
  "/updateVehicleBasic/:vehicleId",
  [autenticateJwt, checkRole(["admin", "user", "superadmin"])],
  vehicleController.updateVehicleBasic
);
// Modificar un vehiculo
router.put(
    "/updateVehicle/:vehicleId",
    [autenticateJwt, checkRole(["admin", "user", "superadmin"])],
    vehicleController.updateVehicle
);
// Eliminar un vehiculo por su id
router.delete(
  "/deleteVehicle/:vehicleId",
  
  vehicleController.deleteVehicle
);

export default router;