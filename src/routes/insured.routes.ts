import { Router } from "express";
import passport from "passport";
import checkRole from "../middlewares/role";
import { InsuredController } from "../controllers/insured.controller";

const insuredController: InsuredController = new InsuredController();
const router = Router();
const autenticateJwt = passport.authenticate("jwt", { session: false });

// Crear un asegurado nuevo
router.post(
  "/createInsured",
  insuredController.addNewInsured
);
// Obtener todos los asegurados
router.get(
  "/getInsureds",
  insuredController.getInsureds
);
// Buscar un asegurado por su dni
router.get(
  "/searchInsured/:insuredDni",
  [autenticateJwt, checkRole(["admin", "user", "superadmin"])],
  insuredController.getInsuredWithDni
);
// Buscar un asegurado por su apellido
router.get(
  "/searchInsureds/:insuredSurname",
  [autenticateJwt, checkRole(["admin", "user", "superadmin"])],
  insuredController.getInsuredsWithSurname
);
// Buscar asegurados por fecha de creacion
router.get(
  "/searchInsureds/:date",
  [autenticateJwt, checkRole(["admin", "user", "superadmin"])],
  insuredController.getInsuredsWithDateCreated
);
// Modificar un asegurado
router.put(
  "/updateInsured/:insuredId",
  [autenticateJwt, checkRole(["admin", "user", "superadmin"])],
  insuredController.updateInsured
);
// Eliminar un usuario por su id
router.delete(
  "/deleteInsured/:insuredId",
  
  insuredController.deleteInsured
);

export default router;