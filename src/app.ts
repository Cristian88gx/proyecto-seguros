import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import passport from 'passport';
import passportMiddleware from './middlewares/passport';
import config from './config/config';
import rolePassport from './middlewares/rolePassport';
import authRoutes from './routes/auth.routes';
import userRoutes from './routes/user.routes';
import insuredRoutes from './routes/insured.routes';
import vehicleRoutes from './routes/vehicle.routes';
import officeRoutes from './routes/office.routes';
import path from 'path';

// Inicializaciones
const app = express();

// Configuraciones
app.set('port',config.port );

// Middlewares
app.use(morgan('dev'));
app.use(cors());
app.use(express.urlencoded({extended: false}));
app.use(express.json());
app.use(passport.initialize());
passport.use(passportMiddleware);
passport.use(rolePassport);

// Routes
app.get('/',(req, res, next) => {
    res.send(`La API esta en http://localhost:${app.get('port')}`);
});
app.use(authRoutes);
app.use('/users',userRoutes);
app.use('/insureds',insuredRoutes);
app.use('/vehicles',vehicleRoutes);
app.use('/offices',officeRoutes);
//app.use('/policies',policyRoutes);


// Folder public files
app.use('/uploads',express.static(path.resolve('uploads')));

export default app;